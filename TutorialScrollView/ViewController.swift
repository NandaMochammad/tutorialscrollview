//
//  ViewController.swift
//  TutorialScrollView
//
//  Created by Nanda Mochammad on 19/03/20.
//  Copyright © 2020 Nanda Mochammad. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //1: create objek of your view
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewScrolling: UIView!
    @IBOutlet var parentView: UIView!
    
    //2: create object of widtConstraint from view
    @IBOutlet weak var viewScrollingWidth: NSLayoutConstraint!
    
    //3: initialize the content of scroll view later
    let data = ["Draft", "Published", "Deleted", "Removed", "Hiden", "Updated"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.bounces = false
        scrollView.showsHorizontalScrollIndicator = false
    }
    
    //4: add this code to prepare the view
    override func viewWillAppear(_ animated: Bool) {

        let buttonWidth: CGFloat = 120
        let padding = CGSize(width: 2, height: 2)
        var buttonPosition = CGPoint(x: padding.width * 0.5, y: padding.height)
        
        viewScrollingWidth.constant = (buttonWidth + padding.width) * CGFloat(data.count)
        
        data.forEach { (data) in
            let button = Bundle.main.loadNibNamed("CustomButton", owner: self, options: nil)?.first as! CustomButtonClass
            
            let buttonIncrement = button.frame.width + padding.width

            button.delegate = self
            button.setContent(title: data)
            
            button.frame.origin = buttonPosition
            buttonPosition.x = buttonPosition.x + buttonIncrement
            
            viewScrolling.addSubview(button)
        }
    }
    
}

//5: Create extension to get the trigger from protocol
extension ViewController: CustomButtonDelegate{
    func buttonTapped(title: String) {
        let colors = [#colorLiteral(red: 0.1215686277, green: 0.01176470611, blue: 0.4235294163, alpha: 1), #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1), #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1), #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1), #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1), #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1), #colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1), #colorLiteral(red: 0.2588235438, green: 0.7568627596, blue: 0.9686274529, alpha: 1), #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)]
        let index = Int.random(in: 0..<colors.count)
        
        parentView.backgroundColor = colors[index]
        print(title)

    }
}
