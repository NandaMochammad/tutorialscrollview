//
//  CustomButtonClass.swift
//  TutorialScrollView
//
//  Created by Nanda Mochammad on 19/03/20.
//  Copyright © 2020 Nanda Mochammad. All rights reserved.
//

import UIKit

protocol CustomButtonDelegate{
    func buttonTapped(title: String)
}

class CustomButtonClass: UIView {

    @IBOutlet weak var buttonTItle: UIButton!
    
    var delegate: CustomButtonDelegate?
    var titleButton = ""
    
    func setContent(title: String){
        titleButton = title
        buttonTItle.setTitle(title, for: .normal)
        
    }
    
    @IBAction func buttonAction(_ sender: Any) {
        self.delegate?.buttonTapped(title: titleButton)
    }
    
}
